# Behaviours

1. exhentai.org returns a 302 Found with a Location header to e-hentai forums

```
< HTTP/1.1 302 Found
< Date: Fri, 01 Feb 2019 13:08:53 GMT
< Server: Apache
< Location: https://forums.e-hentai.org/remoteapi.php?ex=MTU0OTAyNjUzMy0xMTg2NTkwZDk4
< Content-Length: 0
< Content-Type: text/html; charset=UTF-8
< X-Varnish: 648281186
< Age: 0
< Via: 1.1 varnish-v4
< Strict-Transport-Security: max-age=31536000; preload;
< 
```

2. CURL args to login

```bash
curl -v 'https://forums.e-hentai.org/index.php?act=Login&CODE=01' \
		 -H 'Accept: */*' -H 'Referer: https://exhentai.org/' -H 'Origin: https://exhentai.org' \
		 -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3534.4 Safari/537.36' \
		 -H 'DNT: 1' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
		 --data 'referer=https://forums.e-hentai.org/index.php&UserName=${USERNAME}&PassWord=${PASSWORD}&CookieDate=1' \
		 --compressed --cookie-jar /tmp/cookie
		 ```

# Ideas

		 - Log-ins
		 - The idea for log-in, is that the backend should _not_, and should _never_, store authentications on its own. That is the "philosophy" of this backend
		 - Flow:
		 - User sends a POST request with a form to authenticate
		 - `/auth?username=diamondburned&password=asdasdasd`
		 - Server sends the request with that info to exh's server
		 - exh's server responds with the auth codes in the cookies
		 - The server will forward those into the client's cookies, only under a different domain

