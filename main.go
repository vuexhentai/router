package main

import (
	"log"
	"net/http"
	"runtime"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

var (
	r = chi.NewRouter()
)

func init() {
	log.SetFlags(log.Lshortfile)

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Use(middleware.Timeout(time.Minute))
}

func main() {
	r.Post("/auth", AuthenticateHTTP)

	log.Fatalln(http.ListenAndServe("localhost:1337", r))
}

func onError(err error, w *http.ResponseWriter) {
	// Todo:
	// 1. Have this return a 500
	// 2. Log it properly
	// 3. Move it out of this file

	switch err {
	case ErrMissingAuthentication:
		if w != nil {
			writer := *w
			writer.WriteHeader(401)
			writer.Write([]byte(err.Error()))
		}
	default:
		if w != nil {
			writer := *w
			writer.WriteHeader(500)
			writer.Write([]byte(err.Error()))
		}
	}

	_, fn, line, _ := runtime.Caller(1)
	log.Printf("- %s:%d - %v\n", fn, line, err)
}
